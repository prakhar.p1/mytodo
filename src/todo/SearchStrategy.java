package todo;
import java.util.List;

public interface SearchStrategy {

    public List<Todo>searchWithStartWord(String word,List<Todo>currentList);
    
} 
