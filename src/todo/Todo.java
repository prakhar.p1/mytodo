package todo;

import java.time.LocalDate;

public class Todo {
    private LocalDate dateAdded;
    private int id;
    private String task;
    private boolean isCompleted;
    
    public Todo(LocalDate dateAdded, int id, String task) {
        this.dateAdded = dateAdded;
        this.id = id;
        this.task = task;
        this.isCompleted = false;
    }
    @Override
    public String toString() {
        return "Todo [dateAdded=" + dateAdded + ", id=" + id + ", task=" + task + ", isCompleted=" + isCompleted + "]";
    }
    
    public LocalDate getDateAdded() {
        return dateAdded;
    }
    public boolean isCompleted() {
        return isCompleted;
    }
    public int getId() {
        return id;
    }
    public String getTask() {
        return task;
    }
    public void setDateAdded(LocalDate dateAdded) {
        this.dateAdded = dateAdded;
    }
    public void setId(int id) {
        this.id = id;
    }
    public void setTask(String task) {
        this.task = task;
    }
    public void setCompleted(boolean isCompleted) {
        this.isCompleted = isCompleted;
    }

    

    
}
