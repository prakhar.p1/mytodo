package todo;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class TodoManager {
    private List<Todo> todoLists;
    SearchStrategy searchStrategy;
    public TodoManager(){
        todoLists=new ArrayList<Todo>();
        searchStrategy=IterativeSearchStrategy.getInstance();
    }
    public TodoManager(SearchStrategy searchStrategy){
        this.searchStrategy=searchStrategy;
        todoLists=new ArrayList<Todo>();

    }

    public List<Todo> getTodoLists() {
        return todoLists;
    }

    public void setTodoLists(List<Todo> todoLists) {
        this.todoLists = todoLists;
    }
    
    public void addTodo(Todo current){
        List<Todo>currentTodos=getTodoLists();
        currentTodos.add(current);
        setTodoLists(currentTodos);
    }
    public void deleteTodo(int id){
        List<Todo>current=getTodoLists();
        current.removeIf(todo->todo.getId()==id);
        setTodoLists(current);
    }
    public List<Todo> getAllTodosByDate(LocalDate date){
        List<Todo>current=getTodoLists();
        current.removeIf(c->!c.getDateAdded().equals(date));
        return current;

    }
    public void markTodoCompleted(int id){
        List<Todo>current=getTodoLists();
        for(Todo cur:current){
            if(cur.getId()==id){
                cur.setCompleted(true);
                break;
            }
        }
    }





    
}
