package todo;

import java.util.List;

public class IterativeSearchStrategy implements SearchStrategy{
    private static IterativeSearchStrategy instance;
    private IterativeSearchStrategy(){}

    public static IterativeSearchStrategy getInstance(){
        if(instance==null){
            instance=new IterativeSearchStrategy();
        }
        return instance;

    }


    @Override
    public List<Todo> searchWithStartWord(String word,List<Todo>currentList) {
        
        currentList.removeIf(todo->!todo.getTask().startsWith(word));
        return currentList;
    }
    
}
