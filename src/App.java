import java.time.LocalDate;
import java.util.List;

import todo.Todo;
import todo.TodoManager;

public class App {
    private static void printTodoList(TodoManager todoManager){
        List<Todo>cur=todoManager.getTodoLists();
        cur.forEach(System.out::println);
    }
    public static void main(String[] args) {
        TodoManager todoManager=new TodoManager();

        LocalDate date1=LocalDate.of(2024,1,3);
        LocalDate date2=LocalDate.of(2024,1,4);
        LocalDate date3=LocalDate.of(2024,1,5);


        //add 5 tasks
        todoManager.addTodo(new Todo(LocalDate.now(), 0, "Complete Java"));
        todoManager.addTodo(new Todo(date1, 1, "Complete C++"));
        todoManager.addTodo(new Todo(date2, 2, "Complete Node.js"));
        todoManager.addTodo(new Todo(date3, 3, "Complete Node.js"));
        todoManager.addTodo(new Todo(LocalDate.now(), 4, "Complete Python"));


        printTodoList(todoManager);

        System.out.println();



        //delete task with id 2
        todoManager.deleteTodo(2);



        printTodoList(todoManager);
        System.out.println();



        //mark task with id 1 as completed
        todoManager.markTodoCompleted(1);



        printTodoList(todoManager);
        System.out.println();


        //find todos by date (current date is used here)
        List<Todo>todoByCurrentDate=todoManager.getAllTodosByDate(LocalDate.now());
        todoByCurrentDate.forEach(System.out::println);
        
        
        


    }
}
